<?php

//$id = $_POST['id'];

$id = 'qrcode';
//$conn = mysqli_connect('localhost', 'root', '');
include './conecao.php';
if (!$conn) {
    die('Could not connect: ' . mysql_error());
}
$sql = " DROP DATABASE IF EXISTS $id;";
if (mysqli_query($conn, $sql)) {
    $sql = "CREATE DATABASE $id;";
    if (mysqli_query($conn, $sql)) {
        mysqli_select_db($conn, $id);
        $sql = "CREATE TABLE `checkin` (
                  `id` int(11) NOT NULL,
                  `idUsuario` int(11) NOT NULL,
                  `data` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `cargaHoraria` int(11) DEFAULT NULL,
                  `turno` varchar(20) DEFAULT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        if (mysqli_query($conn, $sql)) {
            $sql = "CREATE TABLE `usuario` (
              `id` int(11) NOT NULL,
              `nome` varchar(300) DEFAULT NULL,
              `email` varchar(90) DEFAULT NULL,
              `cidade` varchar(90) DEFAULT NULL,
              `Campus` varchar(500) DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            //    echo '<br>' . $sql . "<br>";
            if (mysqli_query($conn, $sql)) {
                $sql = "ALTER TABLE `checkin`
                    ADD PRIMARY KEY (`id`),
                    ADD KEY `idUsuario` (`idUsuario`);";
                if (mysqli_query($conn, $sql)) {
                    $sql = "ALTER TABLE `usuario`
                    ADD PRIMARY KEY (`id`);";
                    if (mysqli_query($conn, $sql)) {
                        $sql = "    ALTER TABLE `checkin`
                          MODIFY `id` int(11) NOT NULL AUTO_INCREMENT";
                        if (mysqli_query($conn, $sql)) {
                            $sql = "   ALTER TABLE `usuario`
                             MODIFY `id` int(11) NOT NULL AUTO_INCREMENT";

                            if (mysqli_query($conn, $sql)) {
                                $sql = "ALTER TABLE `checkin`
                                   ADD CONSTRAINT `checkin_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;";
                                ?>             
                                <script language='javascript'>
                                    alert('Banco Criado');
                                    window.location.href = '../CSV';
                                </script>
                                <?php

                            } else {
                                echo 'Error creating database: ' . mysqli_error($conn) . "\n";
                            }
                        } else {
                            echo 'Error creating database: ' . mysqli_error($conn) . "\n";
                        }
                    } else {
                        echo 'Error creating database: ' . mysqli_error($conn) . "\n";
                    }
                } else {
                    echo 'Error creating database: ' . mysqli_error($conn) . "\n";
                }
            } else {
                echo 'Error creating database: ' . mysqli_error($conn) . "\n";
            }
        } else {
            echo 'Error creating database: ' . mysqli_error($conn) . "\n";
        }
    } else {
        echo 'Error creating database: ' . mysqli_error($conn) . "\n";
    }
} else {
    echo 'Error creating database: ' . mysqli_error($conn) . "\n";
}