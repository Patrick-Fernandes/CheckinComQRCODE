<?php
/*
Bom dia viajate aqui este É UM GERADOR DE CERTIFICADOs ALTERADO PARA o ifsul, 
 Alteração feita por Patrick Fernandes no dia 25-10-2017 */
$id = $_GET['id'];
//includes para soma das horas
include 'conecao.php';
include 'somaCarga.php';
include './listaUserUnico.php';
$row = mysqli_fetch_assoc($result);



setlocale(LC_ALL, 'pt_BR', 'pt_BR.iso-8859-1', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');
require('../estilo/fpdf/alphapdf.php');
require('../estilo/PHPMailer/class.phpmailer.php');

// --------- Variáveis do BANCO ----- //
$email = utf8_decode($row["email"]);
$nome = utf8_decode($row["nome"]);
$cpf = utf8_decode($row["email"]);
// --------- Variáveis que podem vir de um banco de dados ----- //
$empresa = "Universidade Federal Sul Rio Grandence";
$curso = "1º feira de tecnologias & EAD";
$data = "28/10/2017";
$carga_h = somaCarga($id)."Horas";





//nomenclaruas dos texto que iram aparecer no certificado
$texto1 = utf8_decode($empresa);
$texto2 = utf8_decode("pela participação no " . $curso . " \n realizado em " . $data . " com carga horária total de " . $carga_h . ".");
$texto3 = utf8_decode("Data " . utf8_encode(strftime('%d de %B de %Y', strtotime(date('Y-m-d')))));


$pdf = new AlphaPDF();
// Orientação Landing Page ///
$pdf->AddPage('L');
$pdf->SetLineWidth(1.5);
// desenha a imagem do certificado, não esqueça de alterar  a imgaem na pasta IMG
$pdf->Image('../img/certificado.jpg', 0, 0, 295);


//proximos comentarios são do criador desse script -q
// opacidade total
$pdf->SetAlpha(1);

// Mostrar texto no topo
$pdf->SetFont('Arial', '', 15); // Tipo de fonte e tamanho
$pdf->SetXY(109, 46); //Parte chata onde tem que ficar ajustando a posição X e Y
$pdf->MultiCell(265, 10, $texto1, '', 'L', 0); // Tamanho width e height e posição
// Mostrar o nome
$pdf->SetFont('Arial', '', 30); // Tipo de fonte e tamanho
$pdf->SetXY(20, 86); //Parte chata onde tem que ficar ajustando a posição X e Y
$pdf->MultiCell(265, 10, $nome, '', 'C', 0); // Tamanho width e height e posição
// Mostrar o corpo
$pdf->SetFont('Arial', '', 15); // Tipo de fonte e tamanho
$pdf->SetXY(20, 110); //Parte chata onde tem que ficar ajustando a posição X e Y
$pdf->MultiCell(265, 10, $texto2, '', 'C', 0); // Tamanho width e height e posição
// Mostrar a data no final
$pdf->SetFont('Arial', '', 15); // Tipo de fonte e tamanho
$pdf->SetXY(32, 172); //Parte chata onde tem que ficar ajustando a posição X e Y
$pdf->MultiCell(165, 10, $texto3, '', 'L', 0); // Tamanho width e height e posição

$pdfdoc = $pdf->Output('', 'S');




// ******** Agora vai enviar o e-mail pro usuário contendo o anexo
// ******** e também mostrar na tela para caso o e-mail não chegar
$subject = 'Seu Certificado do Workshop';
$messageBody = "Olá $nome<br><br>É com grande prazer que entregamos o seu certificado.<br>Ele está em anexo nesse e-mail.<br><br>Atenciosamente,<br>Lincoln Borges<br><a href='http://www.lnborges.com.br'>http://www.lnborges.com.br</a>";
$mail = new PHPMailer();
$mail->SetFrom("certificado@lnborges.com.br", "Certificado");
$mail->Subject    = $subject;
$mail->MsgHTML(utf8_decode($messageBody));	
$mail->AddAddress($email); 
$mail->addStringAttachment($pdfdoc, 'certificado.pdf');
$mail->Send();

$certificado = "certificado/$cpf.pdf"; //atribui a variável $certificado com o caminho e o nome do arquivo que será salvo (vai usar o CPF digitado pelo usuário como nome de arquivo)
$pdf->Output(); //Salva o certificado no servidor (verifique se a pasta "arquivos" tem a permissão necessária)
// Utilizando esse script provavelmente o certificado ficara salvo em www.seusite.com.br/gerar_certificado/arquivos/999.999.999-99.pdf (o 999 representa o CPF digitado pelo usuário)

$pdf->Output(); // Mostrar o certificado na tela do navegador
?>
