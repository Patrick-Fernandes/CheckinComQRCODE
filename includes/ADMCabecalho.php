<?php
date_default_timezone_set('America/Sao_Paulo');
header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html >
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap -->
        <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="vendors/iCheck/skins/flat/green.css" rel="stylesheet">
        <!-- bootstrap-progressbar -->
        <link href="vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
        <!-- JQVMap -->
        <link href="vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
        <!-- bootstrap-daterangepicker -->
        <link href="vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="build/css/custom.min.css" rel="stylesheet">
        <title>Área Administrativa Check In </title>
    </head>
    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="ADM" class="site_title"> <span>Check In</span></a>
                        </div>
                        <div class="clearfix"></div>    
                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                            <div class="menu_section">
                                <h3>Menu</h3>
                                <ul class="nav side-menu">
                                    <li><a href="ADM"><i class="glyphicon glyphicon-home"></i> Inicio</a></li>

                                    <li><a><i class="glyphicon glyphicon-user"></i> Usuario <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li>
                                                <a href="Lista_Usuario"><i class="fa fa-fw"></i> Lista de Usuários</a>
                                            </li>
                                            <li>
                                                <a href="Relatorio"><i class="fa fa-fw"></i> Relatório de Presentes</a>
                                            </li> 
                                            <li>
                                                <a href="Adcionar_Carga_Horaria"><i class="fa fa-fw"></i> Adcionar Carga Horaria</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a>
                                            <i class="glyphicon glyphicon-qrcode"></i> Check In
                                            <span class="fa fa-chevron-down"></span>
                                        </a>
                                        <ul class="nav child_menu">
                                            <li>
                                                <a href="Lista_Checkin">
                                                    <i class="fa fa-fw"></i> Lista de Check In
                                                </a>
                                            </li>
                                            <li>
                                                <a href="QR_CODE"><i class="fa fa-fw"></i> Gerador de QR CODE</a>
                                            </li>  
                                        </ul>
                                    </li>
                                    <li>
                                        <a>
                                            <i class="glyphicon glyphicon-download-alt"></i> Utilitarios
                                            <span class="fa fa-chevron-down"></span>
                                        </a>
                                        <ul class="nav child_menu">

                                            <li>
                                                <a href="CSV"><i class="fa fa-fw"></i> Importar arquivo CSV</a>
                                            </li>         
                                            <li>
                                                <a href="DB"><i class="fa fa-fw"></i> Criar Banco de Dados</a>
                                            </li> 
                                        </ul>
                                    </li>
                                    <br>    
                                    <li><a href="home"><i class="glyphicon glyphicon-triangle-left"></i>Voltar para o Checkin</a></li>

                                </ul>
                            </div>

                        </div>
                        <!-- /sidebar menu -->


                        <!-- /menu footer buttons -->
                    </div>
                </div>


                <!-- top navigation -->        <!-- top navigation -->
                <!-- top navigation -->

                <div class="top_nav">
                    <div class="nav_menu">
                        <nav>
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>
                        </nav>
                    </div>
                </div>

                <div class="right_col" role="main">
                    <?php
                    include '../controller/somaCarga.php';
                    