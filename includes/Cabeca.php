<!DOCTYPE html>
<html>
    <head>
        <title>Checkin Feira Tecnologica</title><?php header ('Content-type: text/html; charset=UTF-8'); ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-1.11.2.js"></script>
        <script type="text/javascript">
            jQuery(window).load(function ($) {
                atualizaRelogio();
            });
        </script>
    </head>
    <style>
        select,input,option,button{
            color: #000;
        }
        a,a:hover, a:visited, a:link, a:active{
            color: #fff;
            text-decoration: none;
        }
        a:hover{
            color: red;
        }
        body{   
            background-size: 100%;
            /*background-image: url("view/tela.png") ;*/
            background-repeat: no-repeat; 
            font-family: Arial;
            background-color: #0d5d5c;
            color: #ffffff;
            text-justify: auto;
            font-stretch: expanded; 
            font-weight: 900;

        }
        .lado{
            margin: 3% 0 0 0%; 

        }
        .lado2{
            background: #fff;
            text-align: center;
            padding: 10px  0 1px 0 ;
            color: #0d5d5c;
        }


        #hora
        {
            margin: 0 25% 0 0 ;
        }
        @media screen and (max-width: 1280px) {
            span
            {
                width: 40%;
                /*display: inline-block;*/
                margin: 0;

            }
              #hora
        {
            margin: 0 20% 0 0 ;
        }
         .lado{
            margin: -1% 0 0 0%; 

        }
        }
          @media screen and (min-width: 1600px) {
/*            span
            {
                width: 100%;
                display: inline-block;
                margin: 0;

            }*/
              #hora
        {
            margin: 0 32% 0 0 ;
        }
        }
         @media screen and (min-width: 1440px) {
            iframe
            {
                height: 400px;
            }
        }
    </style><?php date_default_timezone_set('America/Sao_Paulo');?>