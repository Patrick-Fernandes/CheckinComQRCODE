-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 21-Nov-2017 às 18:51
-- Versão do servidor: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qrcode`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `checkin`
--

CREATE TABLE `checkin` (
  `id` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `data` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cargaHoraria` int(11) DEFAULT NULL,
  `turno` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `checkin`
--

INSERT INTO `checkin` (`id`, `idUsuario`, `data`, `cargaHoraria`, `turno`) VALUES
(1, 5, '2017-10-26 17:05:04', 0, 'tarde'),
(2, 15, '2017-10-26 17:05:59', 0, 'tarde'),
(3, 15, '2017-10-26 17:12:01', 0, 'tarde'),
(4, 15, '2017-10-26 17:12:11', 0, 'tarde'),
(5, 15, '2017-10-26 17:12:18', 0, 'tarde'),
(6, 15, '2017-10-26 17:12:29', 0, 'tarde'),
(7, 5, '2017-10-26 17:13:02', 0, 'tarde'),
(8, 5, '2017-10-26 17:13:25', 0, 'tarde'),
(9, 3, '2017-10-27 08:50:20', NULL, NULL),
(10, 2, '2017-10-27 08:51:50', NULL, NULL),
(11, 40, '2017-10-27 08:53:17', NULL, NULL),
(12, 5, '2017-10-27 08:54:18', NULL, NULL),
(13, 21, '2017-10-27 08:55:25', NULL, NULL),
(14, 58, '2017-10-27 08:56:13', NULL, NULL),
(15, 46, '2017-10-27 08:56:23', NULL, NULL),
(16, 35, '2017-10-27 08:58:02', NULL, NULL),
(17, 43, '2017-10-27 08:59:25', NULL, NULL),
(18, 66, '2017-10-27 08:59:35', NULL, NULL),
(19, 1, '2017-10-27 09:00:31', NULL, NULL),
(20, 24, '2017-10-27 09:00:58', NULL, NULL),
(21, 39, '2017-10-27 09:02:04', NULL, NULL),
(22, 16, '2017-10-27 09:02:27', NULL, NULL),
(23, 41, '2017-10-27 09:02:58', NULL, NULL),
(24, 36, '2017-10-27 09:03:51', NULL, NULL),
(25, 33, '2017-10-27 09:05:25', NULL, NULL),
(26, 37, '2017-10-27 09:05:48', NULL, NULL),
(27, 31, '2017-10-27 09:06:19', NULL, NULL),
(28, 45, '2017-10-27 09:07:51', NULL, NULL),
(29, 4, '2017-10-27 09:08:47', NULL, NULL),
(30, 66, '2017-10-27 09:09:22', NULL, NULL),
(31, 66, '2017-10-27 09:09:33', NULL, NULL),
(32, 66, '2017-10-27 09:09:39', NULL, NULL),
(33, 66, '2017-10-27 09:09:57', NULL, NULL),
(34, 12, '2017-10-27 09:10:50', NULL, NULL),
(35, 19, '2017-10-27 09:11:38', NULL, NULL),
(36, 20, '2017-10-27 09:12:00', NULL, NULL),
(37, 63, '2017-10-27 09:12:45', NULL, NULL),
(38, 9, '2017-10-27 09:15:21', NULL, NULL),
(39, 34, '2017-10-27 09:16:18', NULL, NULL),
(40, 23, '2017-10-27 09:16:49', NULL, NULL),
(41, 38, '2017-10-27 09:20:58', NULL, NULL),
(42, 70, '2017-10-27 09:24:45', NULL, NULL),
(43, 61, '2017-10-27 09:32:53', NULL, NULL),
(44, 68, '2017-10-27 09:33:28', NULL, NULL),
(45, 68, '2017-10-27 09:36:19', NULL, NULL),
(46, 28, '2017-10-27 09:48:42', NULL, NULL),
(47, 15, '2017-10-27 10:17:55', NULL, NULL),
(48, 48, '2017-10-27 10:18:18', NULL, NULL),
(49, 67, '2017-10-27 10:19:20', NULL, NULL),
(50, 54, '2017-10-27 10:20:57', NULL, NULL),
(51, 14, '2017-10-27 10:21:31', NULL, NULL),
(52, 62, '2017-10-27 11:32:17', NULL, NULL),
(53, 35, '2017-10-27 13:57:00', NULL, NULL),
(55, 62, '2017-10-27 14:02:17', NULL, NULL),
(56, 43, '2017-10-27 14:03:45', NULL, NULL),
(57, 19, '2017-10-27 14:04:00', NULL, NULL),
(58, 28, '2017-10-27 14:04:11', NULL, NULL),
(59, 45, '2017-10-27 14:04:59', NULL, NULL),
(60, 16, '2017-10-27 14:05:09', NULL, NULL),
(61, 40, '2017-10-27 14:06:04', NULL, NULL),
(62, 24, '2017-10-27 14:06:38', NULL, NULL),
(63, 51, '2017-10-27 14:07:03', NULL, NULL),
(64, 51, '2017-10-27 14:07:19', NULL, NULL),
(65, 67, '2017-10-27 14:07:29', NULL, NULL),
(66, 39, '2017-10-27 14:09:04', NULL, NULL),
(68, 61, '2017-10-27 14:24:12', NULL, NULL),
(69, 61, '2017-10-27 09:24:12', NULL, NULL),
(72, 68, '2017-10-27 15:01:02', NULL, NULL),
(73, 7, '2017-10-27 16:30:39', NULL, NULL),
(74, 14, '2017-10-27 16:47:02', NULL, NULL),
(75, 3, '2017-10-27 15:50:20', NULL, NULL),
(76, 7, '2017-10-27 16:56:25', NULL, NULL),
(77, 15, '2017-10-27 17:05:36', NULL, NULL),
(78, 48, '2017-10-27 17:05:53', NULL, NULL),
(79, 9, '2017-10-27 17:05:59', NULL, NULL),
(80, 31, '2017-10-27 17:06:45', NULL, NULL),
(81, 23, '2017-10-27 17:06:54', NULL, NULL),
(82, 41, '2017-10-27 17:07:01', NULL, NULL),
(83, 33, '2017-10-27 17:07:11', NULL, NULL),
(84, 8, '2017-10-27 17:08:18', NULL, NULL),
(85, 37, '2017-10-27 15:05:48', NULL, NULL),
(86, 34, '2017-10-27 15:16:18', NULL, NULL),
(87, 36, '2017-10-27 17:03:51', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nome` varchar(300) DEFAULT NULL,
  `email` varchar(90) DEFAULT NULL,
  `cidade` varchar(90) DEFAULT NULL,
  `Campus` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `nome`, `email`, `cidade`, `Campus`) VALUES
(1, 'Janine Ochoa', 'Janineochoa916@gmail.com', 'Pelotas', 'Pelotas'),
(2, 'Marla Cristina da Silva Sopeña ', 'marlasopena@gmail.com', 'Pelotas', 'Campus Pelotas'),
(3, 'Cinara Ourique Nascimento', 'cinaraourique@gmail.com', 'Pelotas', 'Ifsul'),
(4, 'Rute Barboza Lacerda', 'rutebl1603@hotmail. com', 'Pelotas', 'Pelotas'),
(5, 'Ricardo Rios Villas Boas', 'ricky.gaucho19@gmail.com', 'Pelotas', 'Campus Pelotas'),
(6, 'Rafael Camargo ferraz', 'Rafacferraz@gmail.com', 'Santana do Livramento', 'Unipampa'),
(7, 'Rafael', 'Rafaccerra@gmail.com', 'Santana do Livramento', 'Unipampa'),
(8, 'Everton Felix', 'evertonfelix@ifsul.edu.br', 'Santana do Livramento', 'IFSul - Campus Santana do Livramento'),
(9, 'César Costa Machado ', 'machado.ifsul@gmail.com', 'Pelotas', 'IFSUL- Pelotas'),
(10, 'Maria Isabel Giusti Moreira', 'Isabelmoreira@gmail.com', 'Pelotas', 'Campus Pelotas'),
(11, 'Cristiane Silveira dos Santos', 'kriskabespanhol@gmail.com', 'Pelotas', 'CAVG'),
(12, 'Marchiori Quadrado de Quevedo', 'marchioriquevedo@gmail.com', 'Pelotas', 'IFSul - Visconde da Graça'),
(13, 'Patrícia Porto Ramos', 'patriciaprifsul@gmail.com', 'Pelotas', 'CAVG'),
(14, 'Ricardo Lemos Sainz', 'ricardosainz@pelotas.ifsul.edu.br', 'Pelotas', 'Campus '),
(15, 'Catiana Dallacort Lodi ', 'cd.lodi@gmail.com ', 'Camargo ', 'POLO UAB CAMARGO '),
(16, 'Valderes Lima Pinto', 'valderespinto@gmail.com', 'Herval', 'Polo UAB'),
(17, 'Edimara Triches', 'edimaratrichestutora@yahoo.com.br', 'Camargo', 'Polo Camargo'),
(18, ' Ismael Elenito Silveira ', 'ismaelesilveira@hotmail.com', 'Balneário Pinhal ', 'Polo de Balneário Pinhal '),
(19, 'Rafael Bertei', 'rafael.bertei@passofundo.ifsul.edu.br', 'Passo Fundo', 'Campus Passo Fundo'),
(20, 'Edimara Sartori', 'edimara.sartori@passofundo.ifsul.edu.br', 'Passo Fundo', 'Campus Passo Fundo'),
(21, 'Juline Fernandes da Silva', 'juline.silva@gmail.com', 'Pelotas', 'IFSUL - Polos Constantina e Balneário Pinhal'),
(22, 'Juline Fernandes da Silva', 'juline.silva@gmail.com', 'Pelotas', 'IFSUL - Polos Constantina e Balneário Pinhal'),
(23, 'Lediane Pereira Marques', 'ledianepmarques@gmail.com', 'São Francisco de Paula ', 'IFSUL - POLO SÃO FRANCISCO DE PAULA'),
(24, 'Lidiane Garcia Bressan', 'lidigbmtm@gmail.com', 'Rosário do Sul', 'Polo Rosário do Sul'),
(25, 'Gabriel da Silva Barros', 'gabrieldasilvabarros1995@gmail.com', 'Pelotas', 'Campus Pelotas'),
(26, 'Gaspar Faria Fialho', 'Gaspar.fialho@hotmail.com', 'Pelotas', 'Campus Pelotas'),
(27, 'Fabiana Zaffalon Ferreira ', 'fabinhazaffalon@gmail.com', 'Pelotas', 'Campus Pelotas'),
(28, 'Fabiane Sarmento Oliveira Fruet', 'fabianefruet@ifsul.edu.br', 'Jaguarão', 'Câmpus avançado Jaguarão - IFSul'),
(29, 'Ricardo Lemos de Souza', 'rcrdsou@hotmail.com', 'Pelotas', 'Campus Pelotas'),
(30, 'Rudinei Domingues da Cruz ', 'LC.rudinei@outlook.com', 'Pelotas', 'Campus Pelotas'),
(31, 'Margot Gisele Zeni Moretto ', 'margotgiselem@bol.com.br ', 'Vila Flores', 'PolO'),
(32, 'Ednilson da Silva Barros', 'ednilson_barros@yahoo.com.br', 'Pelotas', 'Ifsul - Campus Visconde da Graça'),
(33, 'Neusa Maria Carvalho', 'neusabpinhal@gmail.com', ' Balneario Pinhal   ', 'Polo UAB Balneário Pinhal'),
(34, 'Maicon Farias Vieira', 'Maiconfariasvieira@gmail.com', 'Pelotas', 'Campus Pelotas'),
(35, 'Luis Eduardo de Oliveira Holguin', 'lcluisholguin@outlook.com', 'Pelotas ', 'Campus Pelotas'),
(36, 'Meique de Freitas Oliveira', 'meiquefo@hotmail.com', 'Santana da Boa Vista', 'Polo Municipal de Santana da Boa Vista'),
(37, 'Maritânia Bassi Ferreira', 'maritaniabf@gmail.com', 'Pelotas', 'Polos PCF e VF'),
(38, 'Leisa Aparecida Lopes Iepsen', 'lopes.leisa@gmail.com', 'Sapiranga', 'Polo UAB Sapiranga'),
(39, 'Natali Farias Cardoso', 'natalifc@gmail.com', 'Pelotas ', 'Campus Pelotas'),
(40, 'GIANE SÓRIA LOPES', 'gianeslopes@gmail.com', 'HERVAL', 'POLO UNIVERSITÁRIO DE HERVAL'),
(41, 'Katia Rodrigues Pedroso', 'sbv.katia@gmail.com', 'Santana da Boa Vista', 'Polo Santana da Boa Vista'),
(42, 'Margarete Hirdes Antunes', 'npte.margarete@gmail.com', 'Pelotas', 'Reitoria'),
(43, 'Roberta Gonçalves Crizel ', 'robertacrizel@ifsul.edu', 'Jaguarão ', 'Câmpus Avançado Jaguarão '),
(44, 'Ricardo Lemos Sainz', 'ricardosainz@pelotas.ifsul.edu.br', 'Pelotas', 'Campus Pelotas'),
(45, 'Veridiana Krolow Bosenbecker', 'veri.bosenbecker@gmail.com', 'Pelotas', 'Campus Pelotas'),
(46, 'Júlio César M. Ruzicki', 'jcmr@pelotas.ifsul.edu.br', 'Pelotas', 'Campus Pelotas'),
(47, 'Maria de Fátima da Rosa Farias', 'fatimarosaf@yahoo.com.br', 'Pelotas', 'Campus Pelotas'),
(48, 'Samir Casagrande', 'samir-dc@hotmail.com', 'Camargo', 'Polo UAB Camargo'),
(49, 'Gabriela Nunes e Silva', 'gabrielanunes04@gmail.com', 'Pelotas', 'Campus Pelotas'),
(50, 'Lizandro de Souza Oliveira', 'lizandrooliveira@pelotas.ifsul.edu.br', 'Pelotas', 'Campus Pelotas'),
(51, 'Bruna Gonçalves Ribeiro', 'bruna.bgribeiro@gmail.com', 'Pelotas', 'Campus Pelotas'),
(52, 'Igor medeiros atencio', 'Igormedeiros.atencio@yahoo.com.br', 'Pelotas', 'Campus Pelotas'),
(53, 'Paulo Martins Soares', 'Pmspru@gmail.com', 'Pelotas', 'Campus Pelotas'),
(54, 'Gilnei Oleiro Corrêa', 'gilneioleirocorrea@gmail.com', 'Pelotas', 'Campus Pelotas'),
(55, 'Nara cristina fernandes', 'Naraalmeida@pelotas.ifsul.edu.br', 'Pelotas', 'Campus Pelotas'),
(56, 'Claudiane Solano de Paiva', 'dianepaiva@yahoo.com.br', 'Pelotas', 'Campus Pelotas'),
(57, 'Diego Bigliardi Machado', 'diegomachado@pelotas.ifsul.edu.br', 'Pelotas', 'Campus Pelotas'),
(58, 'Shaiana Lemos Silveira', 'xaxa_edu@hotmail.com', 'Pelotas', 'Campus Pelotas'),
(59, 'Luciane Kaster B. Viveiro', 'lucianekaster@gmail.com', 'Pelotas', 'Campus Pelotas'),
(60, 'Patrick Bessa Viveiro', 'professorpatrick@bol.com.br', 'Pelotas', 'Polo Constantina'),
(61, 'Rodrigo da Nascimento Silva', 'rodrigoifsul@gmail.com', 'Pelotas', 'Campus Pelotas'),
(62, NULL, NULL,  NULL, NULL),
(63, 'Raul Filho', 'raulfilho@ifsul.edu.br', 'Pelotas', 'Reitoria'),
(64, NULL, NULL, NULL, NULL),
(65, NULL, NULL, NULL, NULL),
(66, 'luis', 'aksdajs@hohi', 'Pelotas', 'Pelotas'),
(67, 'Patricia Mussi Escobar Iriondo Otero', 'patriciaotero@ifsul.edu.br', 'JaguarÃ£o', 'CÃ¢mpus avanÃ§ado JaguarÃ£o - IFSul'),
(68, 'Rosana Pereira', 'profro50@gmail.com', 'Pelotas', 'Pelotas'),
(69, NULL, NULL, NULL, NULL),
(70, 'Fernando Augusto Brod', 'fernandobrod@cavg.ifsul.edu.br', 'Pelotas', 'CAVG');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `checkin`
--
ALTER TABLE `checkin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idUsuario` (`idUsuario`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `checkin`
--
ALTER TABLE `checkin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `checkin`
--
ALTER TABLE `checkin`
  ADD CONSTRAINT `checkin_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
