
<?php
if (isset($_GET['id'])) {
    $id = $_GET['id'];

    include '../includes/Cabeca.php';
    ?>
    <style>
        input, select {
            width: 100%;
            margin: 1px 0;
            display: inline-block;
        }
        div {
            /*padding: 20px;*/
        }
        label{
            font-size: 1.0em;
        }

    </style>
    <body>
        <h3>Atualização de Usuario</h3>
        <?php
        include '../controller/listaUserUnico.php ';

        if (mysqli_num_rows($result) > 0) {
            $row5 = mysqli_fetch_assoc($result)
            ?>
            <div>
                <form name="form" id="form"  action="Atualizar" method="POST" accept-charset="utf8">
                    <label>Numero de Usuario <?= $row5["id"] ?>
                        <input type="hidden" name="id" value="<?= $row5["id"] ?>">
                    </label>
                    <br>
                    <label for="nome">Nome: </label>
                    <input type="text" name="nome" value="<?= ($row5["nome"]) ?>">
                    <br>
                    <label for="email">E-mail:</label>
                    <input type="email" name="email" value="<?= ($row5["email"]) ?>">
                    <br>
                    <?php include '../controller/listaCidade.php'; ?>

                    <label for="Cidade">Cidade:</label>
                    <select  name="cidade">
                        <?php
                        while ($row = mysqli_fetch_assoc($result2)) {
                            ?>
                            <option value="<?= utf8_encode($row["CIDADE"]) ?>"><?= ($row["CIDADE"]) ?></option>
                        <?php }
                        ?> 
                    </select> 
                    <br>
                    <label for="campus">Campus:</label>
                    <select  name="campus">
                        <?php
                        while ($row = mysqli_fetch_assoc($result4)) {
                            ?>
                            <option value="<?= utf8_encode($row["campus"]) ?>"><?= ($row["campus"]) ?></option>
                        <?php }
                        ?> 
                    </select> 


                    <br>
                    <input type="submit" value="enviar" style="color: #000" /> 
                </form>
            </div>
            <a href="aatt<?= $row5["id"] ?>" >não achei minha cidade</a>
            <?php
        } else {
            ?>
            <script language="javascript">
                window.location.href = "pagina.php";
            </script>
            <?php
        }
    } else {
        ?>
        <script language="javascript">
            window.location.href = "pagina.php";
        </script>
        <?php
    }
    include '../includes/Rodape.php';
    