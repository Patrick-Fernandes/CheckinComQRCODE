<?php
include '../includes/ADMCabecalho.php';
include '../controller/listaUser.php';
?>
<!-- /.row -->
<div class="row">
    
    
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2> Lista de Usuários que compareceram </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                         <thead> 
                                <tD>Nome</tD>
                                <tD>Email</tD>
                                <tD>Turno</tD>
                                <tD>Data</tD>
                                <tD>Cidade</tD>
                                <tD>Campus</tD>
                                </thead>
                        </tr> 
                    </thead>
                    <tbody>
              <?php
                         $sql = "SELECT DISTINCT (`turno`) AND date(date_format(`data`, '%Y-%m-%d')) AS FILTRO ,date_format(`data`, '%Y-%m-%d') 
      AS DATA , U.nome , U.email, U.cidade, U.Campus, turno FROM checkin INNER JOIN usuario AS U ON checkin.idUsuario = U.id
    
      Where U.nome is not null   ORDER BY  DATA,turno,U.nome";
                                $result3 = mysqli_query($conn, $sql);
                                ?>
                              
                                <?php
                                if (mysqli_num_rows($result3) > 0) {
                                    // output data of each row
                                    while ($row2 = mysqli_fetch_assoc($result3)) {
                                        ?>
                                        <tr>                                     
                                            <td><?= $row2["nome"] ?></td>  
                                            <td><?= $row2["email"] ?></td>
                                            <td><?= $row2["turno"] ?></td>                                                                                                                 
                                            <td><?= $row2["DATA"] ?></td>                  
                                            <td><?= $row2["cidade"] ?></td>
                                            <td><?= $row2["Campus"] ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- /.col-lg-4 -->
</div>
<?php
include '../includes/ADMRodape.php';
