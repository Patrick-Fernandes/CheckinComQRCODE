<?php
include '../controller/listaCheckin.php';
include '../includes/ADMCabecalho.php';
?>
<style>
    label,input{
        font-size: 18px;
    }
</style>
<!-- /.row -->
<div class="row">
    <div class="col-md-5 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Adcionar Carga Horaria</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form class="form-horizontal form-label-left"
                      name="myForm" id="myForm"  action="carga" method="POST" required>



                    <div class="form-group">
                        <label class=" col-md-3 col-sm-12 col-xs-12">Carga Horária:</label>
                        <div class="col-md-9 col-sm-12 col-xs-12">
                            <input class="form-control" type="text" name="carga" required >
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="col-md-3 col-sm-12 col-xs-12">Turno:</label>
                        <div class="col-md-9 col-sm-12 col-xs-12">
                            <select  name="turno" class="form-control">
                                <option value="manhã">Manhã</option>
                                <option value="tarde">Tarde</option>
                                <option value="noite">Noite</option>
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class=" col-md-12 col-sm-12 col-xs-12">Data:</label>
                        <div class="col-md-3 col-sm-3 col-xs-11">
                            <input class="form-control" type="text" name="dia" maxlength="2"minlength="2"                                    size="2" required onkeyup="mascara(this, mtel);" placeholder="Dia" >
                        </div>
                        <label class=" col-md-1 col-sm-1 col-xs-1">/</label>
                        <div class="col-md-3 col-sm-3 col-xs-11">
                            <input class="form-control" type="text" name="mes" maxlength="2"minlength="2"size="2"required                                    onkeyup="mascara(this, mtel);" placeholder="Mês" >
                        </div>
                        <label class=" col-md-1 col-sm-1 col-xs-1">/</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input class="form-control" type="text" name="ano" maxlength="4" minlength="4"size="4"required                                    onkeyup="mascara(this, mtel);" placeholder="Ano" ><br><br>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control btn-success" type="submit" value="Enviar" /> 
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>




    <div class="col-md-7 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Lista dos CHECK IN's</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th class="col-md-1 col-sm-1 col-xs-1" >N°</th>
                                <th class="col-md-1 col-sm-1 col-xs-1" >N° Usuário</th>
                                <th>Usuário</th>
                                <th >Data de  Check In</th>
                                <th class="col-md-1 col-sm-1 col-xs-1" >Turno</th>
                                <th class="col-md-1 col-sm-1 col-xs-1" >Carga Horária</th>
                            </tr> 
                        </thead>
                        <tbody>
                            <?php
                            if (mysqli_num_rows($result) > 0) {
                                // output data of each row
                                while ($row2 = mysqli_fetch_assoc($result)) {
                                    ?>
                                    <tr>
                                        <td class="col-md-1 col-sm-1 col-xs-1" ><?= $row2["id"] ?></td>      
                                        <td class="col-md-1 col-sm-1 col-xs-1" ><?= $row2["user"] ?></td>      
                                        <td ><?= $row2["nome"] ?></td>
                                        <td><?= $row2["data"] ?></td>
                                        <td class="col-md-1 col-sm-1 col-xs-1" ><?php
                                            $date = $row2["data"];
                                            $date = new DateTime($date);
                                            $hora = $date->format('H');

                                            if ($hora < 12) {
                                                echo "MANHÃ";
                                            } elseif ($hora >= 12 and $hora <= 18) {
                                                echo "TARDE";
                                            } else if ($hora > 18) {
                                                echo "NOITE";
                                            }
                                            ?></td>
                                        <td class="col-md-1 col-sm-1 col-xs-1" ><?= $row2["carga"] ?></td>

                                    </tr>
                                    <?php
                                }
                            } else {
                                ?>
                                <tr>
                                    <td>Sem nenhum resultado</td>
                                </tr>    
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include '../includes/ADMRodape.php';
