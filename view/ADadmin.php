<?php
include '../includes/ADMCabecalho.php';
include '../controller/listaUserSemNome.php';
$i = 0;
/* * CALCULA QUANTOS VAZIO USUARIOS TEM
 * 
 */
if (mysqli_num_rows($result) > 0) {
    while ($row = mysqli_fetch_assoc($result)) {
        $i++;
    }
}
?>



<!-- top tiles -->
<!-- /top tiles -->
<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $i; ?> Usuarios disponiveis <small></small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="dashboard-widget-content">
                    <h2>Adcionar campos para usuários</h2>

                    <label for="nome">Informe quantidade de Usuarios varios a recem cadastrados</label>
                    <form name="myForm" id="myForm"  action="controller/insereUserVazio.php" method="POST"
                          data-parsley-validate class="form-horizontal form-label-left">
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <input  id="id" type="text" name="id"
                                        required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-4">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Ultimos CheckIn</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Usuário</th>
                            <th>Data</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        include '../controller/listaUltimosCheckin.php';

                        if (mysqli_num_rows($result) > 0) {
                            // output data of each row
                            while ($row2 = mysqli_fetch_assoc($result)) {
                                ?>

                                <tr>
                                    <td><?= $row2["id"] ?></td>
                                    <td><?= $row2["nome"] ?></td>
                                    <td><?= $row2["data"] ?></td>
                                </tr>

                                <?php
                            }
                        } else {
                            ?>  
   <tr>
                                    <td>Sem</td>
                                    <td>nenhum</td>
                                    <td>resultado</td>
                                </tr>
                            <?php
                        }
                        ?>


                    </tbody>
                </table>

            </div>
        </div>
    </div>

</div>







<?php
include '../includes/ADMRodape.php';

