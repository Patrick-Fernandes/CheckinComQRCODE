<?php
include '../includes/ADMCabecalho.php';
include '../controller/listaUser.php';
?>
<!-- /.row -->


<div class="row">
    <div class="col-md-8 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Lista de Usuários</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th class="col-md-1 col-sm-1 col-xs-1" >N°</th>
                            <th>Usuário</th>
                            <th>E-mail</th>
                            <th>Cidade</th>    
                            <th>Campus</th>
                            <th >Total de horas</th>
                            <th class="col-md-1 col-sm-1 col-xs-1" >Ação</th>
                            <th class="col-md-1 col-sm-1 col-xs-1" >Certificado</th>
                        </tr> 
                    </thead>
                    <tbody>
                        <?php
                        if (mysqli_num_rows($result) > 0) {
                            // output data of each row
                            while ($row = mysqli_fetch_assoc($result)) {
                                ?>
                                <tr>
                                    <td class="col-md-1 col-sm-1 col-xs-1" ><?= $row["id"] ?></td>       
                                    <td><?= $row["nome"] ?></td>
                                    <td><?= $row["email"] ?></td>
                                    <td><?= $row["cidade"] ?></td>
                                    <td><?= $row["Campus"] ?></td>
                                    <td ><?= somaCarga($row["id"]) ?></td>
                                    <td class="col-md-1 col-sm-1 col-xs-1" ><a href="AtualizarUser<?= $row["id"] ?>">Editar</a></td>
                                    <td class="col-md-1 col-sm-1 col-xs-1" ><a href="Certificado?id=<?= $row["id"] ?>" target="_blank">
                                            <img src="http://iconbug.com/data/5b/507/52ff0e80b07d28b590bbc4b30befde52.png"
                                                 title="PDF" alt="PDF"
                                                 height="30px" width="30px">
                                        </a></td>
                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td>Sem nenhum resultado</td>
                            </tr>    
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-4 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Campos para usuário</h2> 
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                  <form name="myForm" id="myForm"  action="controller/insereUserVazio.php" method="POST">
                    <label for="nome">Informe quantidade de Usuários varios a recem cadastrados</label>
                    <input type="text" class="form-control" name="id">
                    <input class="form-control btn btn-success" type="submit" value="enviar" /> 
                </form>
            </div>
        </div> 
    </div>
</div>


<?php
include '../includes/ADMRodape.php';
