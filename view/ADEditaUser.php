<?php
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    include '../includes/ADMCabecalho.php';
    include '../controller/listaUser.php';
    ?>
    <style>
        label,input{
            font-size: 18px;
        }
    </style>
    <div class="row"
         <div class="col-md-5 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Adcionar Carga Horaria</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <?php
                    include '../controller/listaUserUnico.php ';

                    if (mysqli_num_rows($result) > 0) {
                        $row = mysqli_fetch_assoc($result)
                        ?>
                        <form class="form-horizontal form-label-left"
                              name="myForm" id="myForm"  id="myForm"  action="Atualizar3" method="POST">

                            <div class="form-group">
                                <label class=" col-md-12 col-sm-12 col-xs-12">Numero de Usuário <?= $row["id"] ?>
                                    <input type="hidden" name="id" value="<?= $row["id"] ?>">
                                </label>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12 col-sm-12 col-xs-12">Nome:</label>   <br>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <input class="form-control" type="text" name="nome" value="<?= $row["nome"] ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 col-sm-12 col-xs-12">E-mail:</label>   <br>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <input class="form-control" type="text" name="email" value="<?= $row["email"] ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12 col-sm-12 col-xs-12">Cidade:</label>   <br>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <input class="form-control" type="text" name="cidade" value="<?= $row["cidade"] ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 col-sm-12 col-xs-12">Campus:</label>   <br>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <input class="form-control" type="text" name="campus" value="<?= $row["Campus"] ?>">
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="col-md-6  col-sm-12 col-xs-12">
                                    <input class="form-control btn-success" type="submit" value="Enviar" /> 
                                </div>
                            </div>
                        </form>
                        <?php
                    }
                    ?>  

                </div>
            </div>
        </div>
    </div>
    <?php
    include '../includes/ADMRodape.php';
} else {
    ?>
    <script language="javascript">
        window.location.href = "home";
    </script>
    <?php
}