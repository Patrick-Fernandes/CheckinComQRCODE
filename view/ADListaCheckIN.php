<?php
include '../includes/ADMCabecalho.php';
include '../controller/listaCidade.php';
include '../controller/filtro.php';
?> 
<!-- /.row -->
<div class="row">


    <div class="col-md-9 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Tabela de  Check In</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th class="col-md-1 col-sm-1 col-xs-1" >N°</th>
                                <th class="col-md-1 col-sm-1 col-xs-1" >N° Usuário</th>
                                <th>Usuário</th>
                                <th>E-mail</th>
                                <th>Data de  Check In</th>
                                <th>Turno</th>
                                <th>Cidade</th>
                                <th>Campus</th>
                                <th>Carga Horária</th>
                            </tr> 
                        </thead>
                        <tbody>
                        <?php
                                /*
                                 * VERIFICAÇÃO PARA MOSTRAR O Q FOI PESQUISADO
                                 */
                                if (isset($_POST['nome']) && isset($_POST['mes']) && isset($_POST['dia']) && isset($_POST['ano']) && isset($_POST['cidade']) && isset($_POST['campus'])) {
                                    IF ($_POST['nome'] == "") {
                                        $nome = "Sem informado";
                                    } else {
                                        $nome = $_POST['nome'];
                                    }
                                    IF ($_POST['mes'] == "") {
                                        $mes = "--";
                                    } else {
                                        $mes = $_POST['mes'];
                                    }
                                    IF ($_POST['dia'] == "") {
                                        $dia = '--';
                                    } else {
                                        $dia = $_POST['dia'];
                                    }
                                    IF ($_POST['ano'] == "") {
                                        $ano = '--';
                                    } else {
                                        $ano = $_POST['ano'];
                                    }
                                    IF ($_POST['cidade'] == "") {
                                        $cidade = 'Sem Informado';
                                    } else {
                                        $cidade = utf8_encode($_POST['cidade']);
                                    }
                                    IF ($_POST['campus'] == "") {
                                        $campus = 'Sem Informado';
                                    } else {
                                        $campus = utf8_encode($_POST['campus']);
                                    }
                                    ?>
                                    <h6>Pesquisado</h6>
                                    <?php
                                    echo "<p>Nome:" . utf8_decode($nome) . "<br> Data: $dia/$mes/$ano <br>Cidade " . utf8_decode($cidade) . "<BR>Campus: " . utf8_decode($campus) . "</p>";
                                }
                                if (mysqli_num_rows($result3) > 0) {
                                    // output data of each row
                                    while ($row2 = mysqli_fetch_assoc($result3)) {
                                        ?>
                                        <tr>
                                            <td class="col-md-1 col-sm-1 col-xs-1" >  <?= $row2["id"] ?></td>      
                                            <td class="col-md-1 col-sm-1 col-xs-1" ><?= $row2["user"] ?></td>      
                                            <td><?= $row2["nome"] ?></td>
                                            <td><?= $row2["email"] ?></td>
                                            <td><?= $row2["data"] ?></td>

                                            <td><?php
                                                $date = $row2["data"];
                                                $date = new DateTime($date);
                                                $hora = $date->format('H');

                                                if ($hora < 12) {
                                                    echo "MANHÃ";
                                                } elseif ($hora >= 12 and $hora < 18) {
                                                    echo "TARDE";
                                                } else if ($hora > 18) {
                                                    echo "NOITE";
                                                }
                                                ?></td>
                                            <td><?= $row2["cidade"] ?></td>
                                            <td><?= $row2["Campus"] ?></td>
                                            <td><?= $row2["cargaHoraria"] ?></td>

                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td>Sem nenhum resultado</td>
                                    </tr>    
                                    <?php
                                }
                                ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


 <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Adcionar Carga Horaria</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form class="form-horizontal form-label-left"
                      name="myForm" id="myForm"  a name="filt"   action="" method="POST">



                    
                      <label for="nome">Nome:</label>        
                        <br>
                        <input type="text" name="nome" class="form-control">      
                        <br>                        <br>
                        <label for="Cidade">Cidade:</label><br>
                        <select  name="cidade" class="form-control">
                            <option value="">IGNORAR</option>
                            <?php
                            while ($row = mysqli_fetch_assoc($result2)) {
                                ?>
                                <option value="<?= $row["CIDADE"] ?>"><?= $row["CIDADE"] ?></option>
                            <?php }
                            ?> 
                        </select> 
                        <br>  <br>
                        <label for="campus">Campus:</label><br>
                        <select  name="campus" class="form-control">
                            <option value="">IGNORAR</option>
                            <?php
                            while ($row = mysqli_fetch_assoc($result4)) {
                                ?>
                                <option value="<?= $row["campus"] ?>"><?= $row["campus"] ?></option>
                            <?php }
                            ?> 
                        </select> 
                        <br>  <br>
                     
                 
                        
                        <div class="form-group">
                        <label class=" col-md-12 col-sm-12 col-xs-12">Data: </label>
                        <input type="text"  name="dia" maxlength="2"minlength="2" size="2"
                               class=" col-md-3 col-sm-11 col-xs-11"
                               onkeyup="mascara(this, mtel);" placeholder="Dia" >
                        <label class=" col-md-1 col-sm-1 col-xs-1">/</label>
                        <input type="text" name="mes" maxlength="2"minlength="2"size="2"
                               class=" col-md-3 col-sm-11 col-xs-11"
                               onkeyup="mascara(this, mtel);" placeholder="Mês" >
                        <label class=" col-md-1 col-sm-1 col-xs-1">/</label>
                        <input type="text" name="ano" maxlength="4" minlength="4"size="4"
                               class=" col-md-3 col-sm-12 col-xs-12"
                               onkeyup="mascara(this, mtel);" placeholder="Ano" ><br><br>
                          </div>
                        
                        
                        <input type="submit" value="Enviar"  class="form-control btn btn-success"/> 
                 
      
                </form>
            </div>
        </div>
    </div>

    <!-- /.col-lg-4 -->
</div>
<?php
include '../includes/ADMRodape.php';
