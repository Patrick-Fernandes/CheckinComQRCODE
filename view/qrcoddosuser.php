<?php
//include '../includes/ADMCabecalho.php';
include '../controller/listaUser.php';
 header('Content-Type: text/html; charset=utf-8');
  ?>
<style>
    body{   
        background-size: 100%;
        /*background-image: url("view/tela.png") ;*/
        background-repeat: no-repeat; 
        font-family: Arial;
        color: #000;
        text-justify: auto;
        font-stretch: expanded; 
        font-weight: 400;
        font-size: 9px;
    }
</style>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12"> 
        <?php
        if (mysqli_num_rows($result) > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {
                $aux = '../estilo/BibliotecaQRCODE/php/qr_img.php?';
                $aux .= 'd=' . $row["id"] . '&';
                $aux .= 'e=H&';
                $aux .= 's=6&';
                $aux .= 't=J';
                ?>
                <div style="float: left; border: 1px solid #000;">
                    <img src="<?= $aux; ?>" />
                    <p align=”center” style="margin: -15px 0 25px 20px">&nbsp;
                        <?= "Nº:&nbsp;" . $row["id"] . "&nbsp;<br>&nbsp;" . ($row["nome"]) ?></p>&nbsp;
                </div>
                <?php
            }
        } else {
            ?>
            Sem nenhum resultado
            <?php
        }
        ?>
    </div>
</div>
<!--siape nº 2422288-->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
    $('#botao').click(function (e) {
        e.preventDefault();
        var texto = $('#texto').val();
        var nivel = $('#nivel').val();
        var pixels = $('#pixels').val();
        var tipo = $('input[name="img"]:checked').val();

        if (texto.length == 0) {
            alert('Informe um texto');
            return(false);
        }
        $('img').attr('src', 'estilo/BibliotecaQRCODE/php/qr_img.php?d=' + texto + '&e=' + nivel + '&s=' + pixels + '&t=' + tipo);
    });
</script>
<?php
include '../includes/ADMRodape.php';
