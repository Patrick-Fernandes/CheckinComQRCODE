<?php
include '../includes/ADMCabecalho.php';
include '../controller/listaUser.php';
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style>
    label,input,h2{
        font-size: 18px;
        color: #000;
    }
    input{
        color: #fff;
    }
    span{
        font-size: 14px;

    }
    
</style>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Criar Banco de Dados ou Resetar banco de dados atual</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form name="myForm" id="myForm"  action="controller/banco.php" method="POST">
                    <input type="submit" value="CRIAR" class="btn-success btn col-md-3 col-sm-12 col-xs-12"/> 
                </form>
            </div>          
        </div>  
    </div>     
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Fazer Backup do Banco de Dados, <b> Caso o banco exista</b></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <span><b>OBS:</b> o banco estará na pasta "BancosDeEventos", 
                    <button class="btn col-md-3 col-sm-12 col-xs-12 btn-dark ">clique aqui para visualizar </button></span>
                <br><br><br>
                <form name="myForm" id="myForm"  action="controller/backupBanco.php" method="POST">
                    <input type="submit" value="BACKUP" class="btn-success btn col-md-3 col-sm-12 col-xs-12" /> 
                </form>
                <br>                            
                <p id="nonenone">
                    Lista dos bancos de dados salvos:
                    <br>
                    <?php
                    $pasta = '../BancosDeEventos/';
                    if (is_dir($pasta)) {
                        $diretorio = dir($pasta);
                        while (($arquivo = $diretorio->read()) !== false) {
                            if ($arquivo != "." && $arquivo != "..") {
                                ?> 
                                <a class='btn btn-defaut btn' href="BancosDeEventos/<?= $arquivo ?>"><?= $arquivo ?></a><br>
                                <?php
                            }
                        }
                    } else {
                        echo 'A pasta não existe.';
                    }
                    ?>
                </p>
            </div>          
        </div>  
    </div>     
</div>
<script>
    $(document).ready(function () {
        document.getElementById('nonenone').style.display = "none";

        $("p.nonenone").css("display: none");
        $("button").click(function () {
            document.getElementById('nonenone').style.display = "block";
        });
    });
</script>
<?php include '../includes/ADMRodape.php';
