<?php
include '../includes/ADMCabecalho.php';
include '../controller/listaUser.php';
?>
<style>
    label,input{
        font-size: 18px;
    }
</style>
<!-- /.row -->
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Gerador de QR CODE</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form class="form-horizontal ">
                    <fieldset>
                        <input type="text" id="texto" placeholder="Texto" class="form-control" />
                        <select id="nivel" class="form-control">
                            <!--				<option value="L">Nível redundância L</option>
                                                                    <option value="M">Nível redundância M</option>
                                                                    <option value="Q">Nível redundância Q</option>-->
                            <option value="H">Nível redundância H</option>
                        </select>
                        <br>
                        <select id="pixels" class="form-control">
                            <!--<option value="4">quadradinho de 4px</option>-->
                            <option value="8">quadradinho de 8px</option>
                            <option value="10">quadradinho de 10px</option>
                            <!--<option value="16">quadradinho de 16px</option>-->
                        </select>
                        <br>
                        <label>
                            <input type="radio" name="img" value="J" />
                            JPEG
                        </label>
                        <label>
                            <input type="radio" name="img" value="P" checked="checked" />
                            PNG
                        </label>
                        <br />
                        <button type="button" id="botao" class="btn btn-success ">Gerar QR Code</button>
                    </fieldset>
                </form>
                <?php
//$aux = 'estilo/BibliotecaQRCODE/php/qr_img.php?';
//			$aux .= 'd=1&';
//			$aux .= 'e=H&';
//			$aux .= 's=8&';
//			$aux .= 't=J';
                ?>
                <div style="float: left; border: 1px solid #000;">
                    <img src="<?php echo $aux; ?>" />
                </div>
                <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
                <script type="text/javascript">
                    $('#botao').click(function (e) {
                        e.preventDefault();
                        var texto = $('#texto').val();
                        var nivel = $('#nivel').val();
                        var pixels = $('#pixels').val();
                        var tipo = $('input[name="img"]:checked').val();
                        if (texto.length == 0) {
                            alert('Informe um texto');
                            return(false);
                        }
                        //				alert('qr_img0.50j/php/qr_img.php?d='+texto+'&e='+nivel+'&s='+pixels+'&t='+tipo);
                        $('img').attr('src', 'estilo/BibliotecaQRCODE/php/qr_img.php?d=' + texto + '&e=' + nivel + '&s=' + pixels + '&t=' + tipo);
                    });
                </script>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Gerador de QR CODE pelos ids dos usuários</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                                <div class="clearfix"></div>

            </div>     
            <div class="x_content">

                <a href="adm/a3"  class="btn btn-success">Gerar QR Code dos ID de todos os usuarios</a>
            </div>     
        </div> 
    </div>
</div>
<?php
include '../includes/ADMRodape.php';
