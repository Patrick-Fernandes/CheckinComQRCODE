<?php
include '../includes/ADMCabecalho.php';
$arq = $_GET['id'];
/* * CALCULA QUANTOS VAZIO USUARIOS TEM
 * 
 */
?>    <!-- /.row -->
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <H2>Lista de dados do Arquivo <?= $arq; ?></H2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php
                $pasta = '../ANEXO/';
                if (is_dir($pasta)) {
                    $diretorio = dir($pasta);
                    while (($arquivo = $diretorio->read()) !== false) {
                        if ($arquivo == $arq . ".csv" || $arquivo == $arq . ".txt") {
                            echo '<br><h4>' . $arquivo . '</h4><br />'
                            . '<h3>Dados</h3>';
                            ?>  
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table bulk_action">
                                    <tbody>    
                                    <thead>
                                        <tr class="headings">
                                            <th>N° linha</th>
                                            <th>Id</th>
                                            <th>Nome</th>
                                            <th>E-mail</th>
                                            <th>Cidade</th>
                                            <th>Campus</th>
                                        </tr> 
                                    </thead>
                                    <?php
                                    $arquivo2 = fopen("../ANEXO/" . $arquivo, "r");
                                    // Lê o conteúdo do arquivo
                                    $i = 0;
                                    while (!feof($arquivo2)) {
                                        $i++;
//                                // Pega os dados da linha
                                        $linha = fgets($arquivo2, 1024);
//                                // Divide as Informações das celular para poder salvar
                                        $dados = explode(';', $linha);
//                                //  // Verifica se o Dados Não é o cabeçalho ou não esta em branco
                                        ?>

                                        <tr>
                                            <td><?= $i ?></td>      
                                            <td><?= utf8_decode($dados[0]) ?></td>      
                                            <td><?= utf8_encode($dados[1]) ?></td>
                                            <td><?= utf8_encode($dados[2]) ?></td>
                                            <td><?= utf8_encode($dados[3]) ?></td>
                                            <td><?= utf8_encode($dados[4]) ?></td>

                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                            <script language="Javascript">
                                function confirmacao(id) {
                                    var resposta = confirm("Deseja remover esse registro?");

                                    if (resposta == true) {
                                        window.location.href = 'ExcluiArquivo-<?= $arquivo ?>';
                                    }
                                }
                            </script>
                            <a class='btn btn-defaut'
                               href="javascript:func()" onclick="confirmacao('3')">Deseja excluir esse arquivo? </a>
                               <?php
                           }
                       }
                   } else {
                       echo 'A pasta não existe.';
                   }
                   ?><BR><BR>
                <a class='btn btn-defaut btn-block' href='CSV'>VOLTAR</a>

            </div>
        </div>
    </div>




</div>
<!-- /.col-lg-8 -->

<?php
include '../includes/ADMRodape.php';

